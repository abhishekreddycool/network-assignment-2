﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomListMenu :MonoBehaviourPunCallbacks

{
    [SerializeField]
    private Transform m_content;
    [SerializeField]
    private RoomListButton m_roomlist;

    private List<RoomListButton>m_buttons = new List<RoomListButton>();
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach(RoomInfo info in roomList)
        {
            if (info.RemovedFromList)
            {
                int a_Index = m_buttons.FindIndex(x => x.RoomInfo.Name == info.Name);
                if(a_Index !=-1)
                {
                    Destroy(m_buttons[a_Index].gameObject);
                    m_buttons.RemoveAt(a_Index);
                }
            }
            else
            {
                RoomListButton button = Instantiate(m_roomlist, m_content);
                if (button != null)
                {
                    button.SetRoomInfo(info);
                    m_buttons.Add(button);
                }
            }
        }
    }
}
