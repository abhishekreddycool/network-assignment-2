﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListButton : MonoBehaviour
{
    [SerializeField]
    private Text m_RoomButton;

    public RoomInfo RoomInfo { get; private set; }
    public void SetRoomInfo(RoomInfo info)
    {
        RoomInfo = info;
        m_RoomButton.text = info.Name;
    }
    public void OnListButtonclicked()
    {
        GameObject a_lobby = GameObject.Find("Lobby");
        a_lobby.GetComponent<Lobby>().SelectedRoom = m_RoomButton.text;
    }
}
