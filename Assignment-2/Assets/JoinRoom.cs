﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoinRoom : MonoBehaviourPunCallbacks
{
    public GameObject m_Lobby;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(m_Lobby.GetComponent<Lobby>().SelectedRoom!="")
        {
            this.gameObject.GetComponent<Button>().interactable = true;
        }
        else
        {
            this.gameObject.GetComponent<Button>().interactable = false;
        }
    }
    public void OnJoinRoom()
    {
        PhotonNetwork.JoinRoom(m_Lobby.GetComponent<Lobby>().SelectedRoom);
    }
    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel(1);
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        OnJoinRoom();
    }
}
