﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviourPunCallbacks
{
    public GameObject m_LobbyPanel;
    public InputField m_roomName;
    public Button m_CreateRoom;
    [System.NonSerialized] public string SelectedRoom = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(m_roomName.text!="")
        {
            m_CreateRoom.GetComponent<Button>().interactable = true;
        }
        else
        {
            m_CreateRoom.GetComponent<Button>().interactable = false;

        }
    }
    public void OncreateRoom()
    {
        CreateRoom();
    }
    private void CreateRoom()
    {
        RoomOptions a_room = new RoomOptions() { IsVisible=true,IsOpen=true,MaxPlayers=2};
        PhotonNetwork.CreateRoom(m_roomName.text,a_room);
        
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        CreateRoom();
    }
    public override void OnCreatedRoom()
    {
        m_LobbyPanel.SetActive(false);
        PhotonNetwork.LoadLevel(1);
    }
}
